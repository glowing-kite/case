# Glowing Kite Case
All screws are M3

## PCB mount
<img src="./docs/glowing-kite-case-front.png" alt="drawing" width="250"/>

The PCB will be screwed to this part. It has a hole on the right for the ethernet port and 2 holes on the top for the LED holder.

## LED holder
<img src="./docs/glowing_kite_light_holder.png" alt="drawing" width="250"/>

This part has 2 gaps for the LED sockets from [Amazon](https://www.amazon.de/gp/product/B01N0FR7VL/ref=ppx_yo_dt_b_asin_title_o01_s00?currency=EUR&ie=UTF8&language=en_GB&psc=1) and 2 holes to screw it on the case

## Display distance holder
<img src="./docs/glowing_kite_distance_holder.png" alt="drawing" width="250"/>

This part is inserted between the PCB and the display.

## Assembled

<img src="./docs/glowing-kite.jpg" alt="drawing" width="250"/>
